-- phpMyAdmin SQL Dump
-- version 5.1.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Apr 25, 2022 at 02:00 PM
-- Server version: 8.0.27
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mxm9894`
--

-- --------------------------------------------------------

--
-- Table structure for table `kvikApartments`
--

create database mxm9894;
use mxm9894;

CREATE TABLE if not exists `kvikApartments` (
  `id` int NOT NULL auto_increment primary key,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `img` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `price` int NOT NULL,
  `balconies` int NOT NULL,
  `rooms` int NOT NULL,
  `owner` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE if not exists  `kvikusers` (
  `id` int NOT NULL auto_increment primary key,
  `username` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

create table if not exists  `kvikowners`(
	`id` int not null auto_increment primary key,
    `userID` int not null,
	`username` varchar(16) NOT NULL,
	`password` varchar(16) NOT NULL,
    `email` varchar(32) not null,
	constraint foreign key(`userID`) references `kvikusers`(`id`)
);

create table if not exists  `kvikExtra`(
	`id` int not null auto_increment primary key,
    `apartmentID` int not null,
	`name` varchar(255) default null,
    `toggle` tinyint(1) default null,
    constraint foreign key(`apartmentID`) references `kvikApartments`(`id`)
);

create table if not exists  `kvikComments`(
		`id` int not null auto_increment primary key,
        `userID` int not null,
        `apartmentID` int not null,
        `comment` varchar(1000) not null,
        `timestamp` timestamp default now(),
        constraint foreign key (`userID`) references `kvikusers`(`id`),
        constraint foreign key (`apartmentID`) references `kvikApartments`(`id`)
);
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
